package com.tapster;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney.Smith
 * Date: 1/25/12
 * Time: 7:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class PreferenceConstants {

    static public final String MY_TAG = "TAPLOG";

    static public final String OpenPitch = "OpenPitch";
    static public final String OpenRoll = "OpenRoll";
    static public final String OpenAzimuth = "OpenAzimuth";

    static public final String ClosedPitch = "ClosedPitch";
    static public final String ClosedRoll = "ClosedRoll";
    static public final String ClosedAzimuth = "ClosedAzimuth";

    static public final String TapOpenThreshold = "TapOpenThreshold";

	static public final String PourUrl = "PourUrl";
    static public final String AudioClipsFolder = "audio_clips";




}
