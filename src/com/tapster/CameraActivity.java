package com.tapster;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.*;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.media.ExifInterface;
import android.media.MediaRecorder;
import android.os.*;
import android.util.Log;
import android.view.*;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CameraActivity extends Activity {

	String _soundFilename;
	String _photoFilename;
	Preview preview;
	Button buttonClick;
	TextView countdownTextView;
	LinearLayout _layout;
	private MediaRecorder mRecorder = null;
	private boolean _takePicture = false;
	private boolean _recordSound = false;
	long _pourStartTime;
	Boolean _isRecordingAudio = false;
	MyCountdown _myCountdown;
	Boolean _useFrontFacingCamera = true;
	Intent _returnIntent = new Intent();
	public CameraActivity() {
	}

	public void set_soundFileName(String name) {
		_soundFilename = name;
		_returnIntent.putExtra("SOUND_FILE", name);
		if(!_takePicture){

			setResult(RESULT_OK, _returnIntent);
			finish();
		}
		else if(_takePicture && _photoFilename!= null){
			setResult(RESULT_OK, _returnIntent);
			finish();
		}
	}

	public void set_photoFileName(String name) {
		_photoFilename = name;
		_returnIntent.putExtra("PHOTO_FILE", name);
		if(!_recordSound){
			setResult(RESULT_OK, _returnIntent);
			finish();
		}
		else if(_recordSound && _soundFilename != null){
			setResult(RESULT_OK, _returnIntent);
			finish();
		}
	}

	private class MyCountdown extends CountDownTimer {

		public MyCountdown(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		public void onTick(long millisUntilFinished) {
			Log.d(PreferenceConstants.MY_TAG, "millisUntilFinished " + millisUntilFinished);
			countdownTextView.setText("seconds remaining: " + millisUntilFinished / 1000);
		}

		public void onFinish() {
			Log.d(PreferenceConstants.MY_TAG, "CountdownTimer onFinish");
			stopRecording();
			countdownTextView.setText("done!");
			if (preview != null && preview.camera != null) {
				preview.camera.takePicture(null, null, myPictureCallback_JPG);
			}

		}
	}

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);



		setContentView(R.layout.camera);

//		buttonClick = (Button) findViewById(R.id.buttonClick);
//		buttonClick.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				preview.camera.takePicture(null, null,
//						jpegCallback);
//			}
//		});

		countdownTextView = (TextView) findViewById(R.id.countdown);
		_layout = (LinearLayout) findViewById(R.id.camera_acitivy_layout);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			_takePicture = extras.getBoolean("TAKE_PHOTO", false);
			_recordSound = extras.getBoolean("RECORD_AUDIO", false);
			_pourStartTime = extras.getLong("POUR_START_TIME", 0);

		}
		_myCountdown = new MyCountdown(5000, 1000);
		if (_recordSound) {

			startRecording(getSoundFilename());
		}
		if (_takePicture) {
			preview = new Preview(this, this);
			((FrameLayout) findViewById(R.id.preview)).addView(preview);
		}
		_myCountdown.start();


	}

	String getSoundFilename() {
		//construct name for sound file
		String filepath = getExternalFilesDir(null).getPath();
		String subfolder = "/recorded";
		File outputDirectory = new File(filepath + subfolder);
		outputDirectory.mkdirs();
		String filename = "/" + _pourStartTime + ".3gp";
		String fullPathToFile = filepath + subfolder + filename;
		return fullPathToFile;
	}

	String getPhotoFilename() {
		//construct name for sound file

		String filepath = getExternalFilesDir(null).getPath();
		String subfolder = "/photos";
		File outputDirectory = new File(filepath + subfolder);
		outputDirectory.mkdirs();
		String filename = "/" + _pourStartTime + ".jpg";
		String fullPathToFile = filepath + subfolder + filename;
		return fullPathToFile;
	}
	Thread _photoProcessingThread;
	/**
	 * Handles data for jpeg picture
	 * this is some hacky shit to rotate the pic cuz on the devices i have it
	 * stores the pic rotated
	 */
	PictureCallback myPictureCallback_JPG = new PictureCallback() {

		public void onPictureTaken(final byte[] arg0, Camera arg1) {
			_photoProcessingThread = new Thread("ProcessPhoto")
			{
				@Override
				public void run(){
					Log.d(PreferenceConstants.MY_TAG,"processing image file on thread " + Thread.currentThread().getName());
					String filename = getPhotoFilename();
					BitmapFactory.Options options = new BitmapFactory.Options();
					options.inSampleSize = 6;
					options.inDither = false; // Disable Dithering mode
					options.inPurgeable = true; // Tell to gc that whether it needs free
					// memory, the Bitmap can be cleared
					options.inInputShareable = true; // Which kind of reference will be
					// used to recover the Bitmap
					// data after being clear, when
					// it will be used in the future
					options.inTempStorage = new byte[32 * 1024];
					options.inPreferredConfig = Bitmap.Config.RGB_565;
					Bitmap bMap = BitmapFactory.decodeByteArray(arg0, 0, arg0.length, options);
					float orientation;
					// others devices
					if (bMap.getHeight() < bMap.getWidth()) {
						orientation = 270;
					} else {
						orientation = 0;
					}

					Bitmap bMapRotate;
					if (orientation != 0) {
						Matrix matrix = new Matrix();
						matrix.postRotate(orientation);
						bMapRotate = Bitmap.createBitmap(bMap, 0, 0, bMap.getWidth(),
								bMap.getHeight(), matrix, true);
					} else
						bMapRotate = Bitmap.createScaledBitmap(bMap, bMap.getWidth(),
								bMap.getHeight(), true);


					FileOutputStream out;
					try {
						out = new FileOutputStream(filename);
						bMapRotate.compress(Bitmap.CompressFormat.JPEG, 90, out);
						if (bMapRotate != null) {
							bMapRotate.recycle();
							bMapRotate = null;
						}
					} catch (FileNotFoundException e) {
						Log.d(PreferenceConstants.MY_TAG, "shittin", e);
					}
					set_photoFileName(filename);


				}
			};
			_photoProcessingThread.start();
		}

	};

	PictureCallback jpegCallback = new PictureCallback() {
		public void onPictureTaken(byte[] data, Camera camera) {
			String filename = getPhotoFilename();
			FileOutputStream outStream = null;
			try {

				outStream = new FileOutputStream(filename);
				outStream.write(data);
				outStream.close();
				ExifInterface exif = new ExifInterface(filename);
				String exifOrientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
				Log.d(PreferenceConstants.MY_TAG, "onPictureTaken - wrote" + data.length + "bytes to file:  " + filename);
				Log.d(PreferenceConstants.MY_TAG, "exifOrientation: " + exifOrientation);

			} catch (FileNotFoundException e) {
				Log.e(PreferenceConstants.MY_TAG, "Error in onPictureTaken", e);

			} catch (IOException e) {
				Log.e(PreferenceConstants.MY_TAG, "Error in onPictureTaken", e);
			} finally {
			}
			set_photoFileName(filename);

		}
	};

	public static void setCameraDisplayOrientation(Activity activity,
												   int cameraId, android.hardware.Camera camera) {
		android.hardware.Camera.CameraInfo info =
				new android.hardware.Camera.CameraInfo();
		android.hardware.Camera.getCameraInfo(cameraId, info);
		int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
		int degrees = 0;
		switch (rotation) {
			case Surface.ROTATION_0:
				degrees = 0;
				break;
			case Surface.ROTATION_90:
				degrees = 90;
				break;
			case Surface.ROTATION_180:
				degrees = 180;
				break;
			case Surface.ROTATION_270:
				degrees = 270;
				break;
		}

		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360;  // compensate the mirror
			Log.d(PreferenceConstants.MY_TAG, "setCameraDisplayOrientation for Front camera. id=" + cameraId);
		} else {  // back-facing
			result = (info.orientation - degrees + 360) % 360;
			Log.d(PreferenceConstants.MY_TAG, "setCameraDisplayOrientation for Rear camera. id=" + cameraId);
		}
		camera.setDisplayOrientation(result);
	}

	Camera getCamera() {
		Camera returnVal = null;
		int cameraId = 0;
		Camera.CameraInfo info =
				new Camera.CameraInfo();
		int num = Camera.getNumberOfCameras();
		Log.d(PreferenceConstants.MY_TAG, "This phone has " + num + " cameras");
		for (int i = 0; i < num; i++) {
			android.hardware.Camera.getCameraInfo(i, info);
			cameraId = i;
			if (_useFrontFacingCamera && info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {

				break;
			}
		}
		returnVal = Camera.open(cameraId);
		setCameraDisplayOrientation(this, cameraId, returnVal);
		return returnVal;


	}

	private class Preview extends SurfaceView implements SurfaceHolder.Callback {
		boolean isPreviewRunning = false;
		SurfaceHolder mHolder;
		public Camera camera;
		Activity parentActivity;

		Preview(Context context, Activity a) {
			super(context);
			parentActivity = a;
			// Install a SurfaceHolder.Callback so we get notified when the
			// underlying surface is created and destroyed.
			mHolder = getHolder();
			mHolder.addCallback(this);
			mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}

		public void surfaceCreated(SurfaceHolder holder) {
			// The Surface has been created, acquire the camera and tell it where
			// to draw.
			Log.d(PreferenceConstants.MY_TAG, "surfaceCreated");
			try {
				camera = getCamera();
				camera.setPreviewDisplay(holder);
				camera.setPreviewCallback(new Camera.PreviewCallback() {

					public void onPreviewFrame(byte[] data, Camera arg1) {
						Preview.this.invalidate();
					}
				});
			} catch (IOException e) {
				Log.e(PreferenceConstants.MY_TAG, "Failed to open camera", e);
				_takePicture = false;
			} catch (Exception e) {
				Log.e(PreferenceConstants.MY_TAG, "Failed to open camera", e);
				_takePicture = false;
			}
		}

		public void surfaceDestroyed(SurfaceHolder holder) {
			// Surface will be destroyed when we return, so stop the preview.
			// Because the CameraDevice object is not a shared resource, it's very
			// important to release it when the activity is paused.
			Log.d(PreferenceConstants.MY_TAG, "surfaceDestroyed");
			if (camera == null) return;
			camera.stopPreview();
			camera.release();
			camera = null;
		}

		public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {

			if (camera == null) {
				Log.d(PreferenceConstants.MY_TAG, "Camera is null in surfaceChanged, exiting");
				return;
			}
			try {
				if (isPreviewRunning) {
					Log.d(PreferenceConstants.MY_TAG, "surfaceChanged preview is running");
					camera.stopPreview();
				}
				Camera.Parameters parameters = camera.getParameters();
				List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();
				int rotation = parentActivity.getWindowManager().getDefaultDisplay().getRotation();
				int degrees = 0;
				switch (rotation) {
					case Surface.ROTATION_0:
						degrees = 0;
						camera.setDisplayOrientation(90);
						break;
					case Surface.ROTATION_90:
						degrees = 90;
						break;
					case Surface.ROTATION_180:
						degrees = 180;
						break;
					case Surface.ROTATION_270:
						degrees = 270;
						camera.setDisplayOrientation(180);
						break;
				}


				if (rotation == Surface.ROTATION_0) {
					parameters.setPreviewSize(h, w);

				}


				Log.d(PreferenceConstants.MY_TAG, "number of supported preview sizes = " + previewSizes.size());
				for (Camera.Size s : previewSizes) {
					Log.d(PreferenceConstants.MY_TAG, s.height + "," + s.width);
				}
				parameters.setPreviewSize(previewSizes.get(0).width, previewSizes.get(0).height);
				camera.setParameters(parameters);
				previewCamera();
			} catch (Exception e) {
				Log.e(PreferenceConstants.MY_TAG, "surfaceChanged", e);

			}
		}

		public void previewCamera() {
			try {
				camera.setPreviewDisplay(mHolder);
				camera.startPreview();
				isPreviewRunning = true;
			} catch (Exception e) {
				Log.d(PreferenceConstants.MY_TAG, "Cannot start preview", e);
			}
		}

		@Override
		public void draw(Canvas canvas) {
			super.draw(canvas);
			Paint p = new Paint(Color.RED);
			canvas.drawText("PREVIEW", canvas.getWidth() / 2,
					canvas.getHeight() / 2, p);
		}

	}

	protected void onPause() {

		super.onPause();
		if (mRecorder != null) {
			Log.d(PreferenceConstants.MY_TAG, "releasing camera from activity onPause");
			mRecorder.release();
			mRecorder = null;
		}
	}

	private void startRecording(final String outputFile) {

		if (_isRecordingAudio)
			return;

		Log.d(PreferenceConstants.MY_TAG, "Recording sound file: " + outputFile);

		_layout.setBackgroundColor(Color.RED);
		Thread t = new Thread()
		{
			@Override
			public void run(){
				mRecorder = new MediaRecorder();
				mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
				mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
				mRecorder.setOutputFile(outputFile);
				mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

				try {
					mRecorder.prepare();
					mRecorder.start();
					_isRecordingAudio = true;
				} catch (IOException e) {
					_layout.setBackgroundColor(Color.BLACK);
					Log.e(PreferenceConstants.MY_TAG, "prepare() failed");
					_isRecordingAudio = false;
				}
			}
		};
		t.start();


	}


	private void stopRecording() {
		if (mRecorder == null) return;

		mRecorder.stop();
		_layout.setBackgroundColor(Color.BLACK);
		mRecorder.release();
		_isRecordingAudio = false;
		mRecorder = null;
		set_soundFileName(getSoundFilename());


	}

}