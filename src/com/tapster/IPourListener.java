package com.tapster;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 1/23/12
 * Time: 10:07 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IPourListener {
    public void onTapOpen();
    public void onTapClose();
}
