package com.tapster;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.tapster.utils.ILog;

import javax.inject.Inject;
import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 3/2/12
 * Time: 10:06 PM
 */
public class Dashboard extends ListActivity implements IPourListener{
	@Inject
	private ILog _log;
	private PourButton m_PourButton = null;
	private ProgressDialog m_ProgressDialog = null;
	private ArrayList<Stat> m_stats = null;
	private OrderAdapter m_adapter;
	private Runnable getStatsRunnable;
	long _pourStartTime;
	long _pourEndTime;
	DataAccess _dataAccess;
	IPourEventManager _pourEventManager;
	Stats _stats;
	LinearLayout _dashboardLayout;
	Beer _currentBeer;
	TextView _beerName;
	TextView _beerMaker;
	TextView _beerDescription;
	MyMediaPlayer _mediaPlayer;
	boolean _tapIsOpen = false  ;
	UUID _pourIdFromSource;
	RandomUUID _idGenerator = new RandomUUID();

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);



		setContentView(R.layout.dashboard);
		m_stats = new ArrayList<Stat>();
		MainApplication app = (MainApplication) getApplication();
		app.getObjectGraph().inject(this);

		_dataAccess = app.get_dataAccess();
		_stats = new Stats(_dataAccess);
		_pourEventManager = app.get_pourEventManager();
		_pourEventManager.AddListener(this);
		this.m_adapter = new OrderAdapter(this, R.layout.row, m_stats);
		setListAdapter(this.m_adapter);
		_dashboardLayout = (LinearLayout)findViewById(R.id.dashboardlayout);
		_beerName = (TextView)findViewById(R.id.tvBeerName);
		_beerMaker = (TextView)findViewById(R.id.tvBeerMaker);
		_beerDescription = (TextView)findViewById(R.id.tvBeerDescription);

		getStatsRunnable = new Runnable(){
			public void run() {
				getStats();
			}
		};
		_mediaPlayer = new MyMediaPlayer(this,getExternalFilesDir(null).getPath() + "/" + PreferenceConstants.AudioClipsFolder);

		File outputDirectory = new File(getExternalFilesDir(null).getPath() + "/recorded/");
		outputDirectory.mkdirs();
	}

	private void RetrieveStats() {
		Thread thread =  new Thread(getStatsRunnable, "MagentoBackground");
		thread.start();
		m_ProgressDialog = ProgressDialog.show(Dashboard.this,
				"Please wait...", "Retrieving data ...", true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.calibrate, menu);
		return true;
	}
	@Override
	public void onActivityResult(int reqCode, int resultCode, Intent data) {

		super.onActivityResult(reqCode, resultCode, data);
		_log.d("onActivityResult reqCode=" + reqCode + " resultCode=" + resultCode);
		switch (reqCode) {
			case (66) :

				if(resultCode == RESULT_OK){
					String photoFile=data.getStringExtra("PHOTO_FILE");
					_log.d("PHOTO_FILE = " + photoFile);
					String soundFile=data.getStringExtra("SOUND_FILE");
					_log.d("SOUND_FILE = " + soundFile);
					if(photoFile != null)
					{
						File f = new File(photoFile);
						if(f.exists())
						{
							//upload sound file to server
							_dataAccess.UploadFile(_pourIdFromSource, photoFile, MediaType.Photo, true);
						}
					}
					if(soundFile != null)
					{
						File f = new File(soundFile);
						if(f.exists())
						{
							//upload sound file to server
							_dataAccess.UploadFile(_pourIdFromSource, soundFile, MediaType.Audio, true);
						}
					}
				}
				break;
		}
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
			case R.id.calibrate:
				Intent calibrationScreen = new Intent(this, CalibrationActivity.class);
				startActivity(calibrationScreen);
				return true;
			case R.id.pourLog:
				Intent pourLogScreen = new Intent(this, PourLogActivity.class);
				startActivity(pourLogScreen);
				return true;
			case R.id.changeKeg:
				Intent changeKegScreen = new Intent(this, ChangeKegActivity.class);
				startActivity(changeKegScreen);
				return true;
			case R.id.emptyKeg:
				Intent emptyKegScreen = new Intent(this, EmptyKegActivity.class);
				startActivity(emptyKegScreen);
				return true;
			case R.id.appSettings:
				Intent settingsScreen = new Intent(this, SettingsActivity.class);
				startActivity(settingsScreen);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	protected void onResume() {
		super.onResume();
		//if pref is to show button
		if(PreferenceManager.getDefaultSharedPreferences(this).getBoolean("pref_pour_button",false))
		{
			if(m_PourButton == null)
			{
				m_PourButton = new PourButton(this);
				_dashboardLayout.addView(m_PourButton,0,new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT,
						0));
			}else
			{
				m_PourButton.setVisibility(View.VISIBLE);
			}
			//if the button is there, don't respond to phone movement
			_pourEventManager.RemoveListener(this);
		}
		else
		{
			if(m_PourButton != null)m_PourButton.setVisibility(View.GONE);
			_pourEventManager.AddListener(this);
		}
        RetrieveStats();



	}
	protected void onPause() {
		super.onPause();
		_pourEventManager.RemoveListener(this);
	}

	protected void onDestroy() {
		super.onDestroy();
		_pourEventManager.RemoveListener(this);
		_mediaPlayer.Stop();
	}
	private void getStats(){
		try{
			_currentBeer = _dataAccess.GetCurrentBeer();
			m_stats = new ArrayList<Stat>();
			m_stats.add(_stats.GetNumberOfPoursFromCurrentKeg());
			m_stats.add(_stats.GetLastPourTime());
			//m_stats.add(_stats.GetTimeSinceLastPour());
			m_stats.add(_stats.GetCurrentKegCommissionDate());
			m_stats.add(_stats.GetAveragePourTimeInSeconds());
			m_stats.add(_stats.GetTotalTapOpenTimeForCurrentKegInSeconds());
		} catch (Exception e) {
			Log.e("BACKGROUND_PROC", e.getMessage());
		}
		runOnUiThread(returnRes);
	}
	private Runnable returnRes = new Runnable() {
		public void run() {
			m_adapter.clear();
			if(m_stats != null && m_stats.size() > 0){
				m_adapter.notifyDataSetChanged();
				for(int i=0;i< m_stats.size();i++)
					m_adapter.add(m_stats.get(i));
			}
			_beerDescription.setText(_currentBeer.get_beerDescription());
			_beerMaker.setText(_currentBeer.get_beerMaker());
			_beerName.setText(_currentBeer.get_beerName());
			m_ProgressDialog.dismiss();
			m_adapter.notifyDataSetChanged();
		}
	};

	public void onTapOpen() {
		_dashboardLayout.setBackgroundColor(Color.WHITE);
		_pourStartTime =  System.currentTimeMillis();
		_tapIsOpen = true;
		if(PreferenceManager.getDefaultSharedPreferences(this).getBoolean("pref_play_sound",false))
		{
			_mediaPlayer.Play();
		}
	}

	public void onTapClose() {
		if(!_tapIsOpen)return;
		_tapIsOpen = false;
		_pourEndTime = System.currentTimeMillis();
		//write pour event on separate thread and send a unique id with it

		_pourIdFromSource = _idGenerator.GetRandomUUID();
		_dataAccess.WritePourEvent(_pourIdFromSource, _pourStartTime,_pourEndTime, true);
		_dashboardLayout.setBackgroundColor(Color.BLACK);


		_mediaPlayer.Stop();
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		Boolean recordPref = sharedPref.getBoolean("pref_record", false);
		Boolean photoPref = sharedPref.getBoolean("pref_photo",false);
		if(recordPref || photoPref){
			Intent cameraActivity = new Intent(this, CameraActivity.class);
			cameraActivity.putExtra("POUR_START_TIME",_pourStartTime );
			cameraActivity.putExtra("TAKE_PHOTO", photoPref);
			cameraActivity.putExtra("RECORD_AUDIO", recordPref);
			//start activity to capture media
			startActivityForResult(cameraActivity,66);
		}
        RetrieveStats();

//
//
//		if(!recordPref)
//		{
//			int pourId = _dataAccess.WritePourEvent(_pourStartTime, _pourEndTime);
//		}
		//RetrieveStats();



	}
//	private void writePourEventAndSendMediaFile(String fullPathToFile)
//	{
//
//		int pourId = _dataAccess.WritePourEvent(_pourStartTime, _pourEndTime);
//		File f = new File(fullPathToFile);
//		if(f.exists())
//		{
//			//upload sound file to server
//			_dataAccess.UploadFile(pourId, fullPathToFile);
//		}
//	}

	private class OrderAdapter extends ArrayAdapter<Stat> {

		private ArrayList<Stat> items;

		public OrderAdapter(Context context, int textViewResourceId, ArrayList<Stat> items) {
			super(context, textViewResourceId, items);
			this.items = items;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.row, null);
			}
			Stat o = items.get(position);
			if (o != null) {
				TextView tt = (TextView) v.findViewById(R.id.toptext);
				TextView bt = (TextView) v.findViewById(R.id.bottomtext);
				if (tt != null) {
					tt.setText(o.getStatName());                            }
				if(bt != null){
					bt.setText(o.getStatValue());
				}
			}
			return v;
		}
	}

	class PourButton extends Button {
		boolean mStartRecording = true;

		OnClickListener clicker = new OnClickListener() {
			public void onClick(View v) {
				if(_tapIsOpen)
				{
					onTapClose();
					setText("Open");

				}
				else
				{
					onTapOpen();
					setText("Close");
				}

			}
		};

		public PourButton(Context ctx) {
			super(ctx);
			setText("Open");
			setOnClickListener(clicker);
		}
	}
}