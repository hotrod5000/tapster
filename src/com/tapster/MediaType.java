package com.tapster;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 2/15/13
 * Time: 10:36 PM
 */
public enum MediaType {
	Audio,
	Video,
	Photo
}
