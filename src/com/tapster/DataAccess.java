package com.tapster;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.os.Handler;
import android.util.Log;

import java.util.Date;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 2/13/12
 * Time: 9:23 PM
 */
public class DataAccess {

	private Boolean _initialized = false;
	Context _context;
	EventDataSQLHelper _eventsData;
	SQLiteDatabase _database;
	//CloudDataAccess _cloudDataAccess;
	CodeSmithDataAccess _cloudDataAccess;
	// Need handler for callbacks to the UI thread
	final Handler mHandler = new Handler();

	// Create runnable for posting
	final Runnable mUpdateResults = new Runnable() {
		public void run() {
			set_initialized(true);
		}
	};
	class UploadFileRunnable implements  Runnable{
		UUID _pourId;
		String _fileName;
		MediaType _mediaType;

		UploadFileRunnable(UUID _pourId, String _fileName, MediaType _mediaType) {
			this._pourId = _pourId;
			this._fileName = _fileName;
			this._mediaType = _mediaType;
		}

		public void run(){
			_cloudDataAccess.UploadFile(_pourId, _fileName, _mediaType);
		}
	}
	class PourEventRunnable implements Runnable{
		UUID _pourId;
		long _startTime;
		long _endTime;
		public PourEventRunnable(UUID pourId, long startTime, long endTime)
		{
			_pourId = pourId;
			_startTime = startTime;
			_endTime = endTime;
		}
		public void run() {
			//get the beer that is currently pouring
			int currentBeer = GetCurrentBeerId();
			ContentValues values = new ContentValues();
			values.put("pour_start_time", _startTime);
			values.put("pour_end_time", _endTime);
			values.put("beer", currentBeer);
			_database.insert(EventDataSQLHelper.TABLE, null, values);
			_cloudDataAccess.WritePourEvent(_pourId, new Date(_startTime), new Date(_endTime));
		}
	};
	public DataAccess(Context context) {
		_context = context;
		_eventsData = new EventDataSQLHelper(_context);
		_cloudDataAccess = new CodeSmithDataAccess(_context, this);
		//do this off of ui thread
		Thread thread = new Thread("DataAccess init thread")
		{
			@Override
			public void run(){
				_database = _eventsData.getWritableDatabase();
				_cloudDataAccess.DiscoverUrls();
				mHandler.post(mUpdateResults);
			}

		};
		thread.start();


	}

	public void UploadFile(UUID pourId, String fileName, MediaType mediaType, Boolean async)
	{
		UploadFileRunnable r = new UploadFileRunnable(pourId, fileName, mediaType);
		if(async){
			Thread t = new Thread(r, "UploadFile thread");
			t.start();

		}else
		{
			r.run();
		}


	}
	public Cursor GetAllPourEvents() {
		return _database.query(EventDataSQLHelper.TABLE, null, null, null, null,
				null, null);
	}

	public void WritePourEvent(UUID pourId, long startTime, long endTime, Boolean async) {
		PourEventRunnable r = new PourEventRunnable(pourId, startTime, endTime);
		if(async)
		{
			Thread t = new Thread(r, "WritePourEvent thread");
			t.start();
		}
		else
		{
			r.run();
		}

	}

	public void AddBeer(String beerMaker, String beerName, String description) {

		ContentValues values = new ContentValues();
		values.put("beer_maker", beerMaker);
		values.put("beer_name", beerName);
		values.put("description", description);
		_database.insert("beers", null, values);

	}

	public void Close() {
		_eventsData.close();

	}

	public Cursor GetBeersWithNameLike(String beerName) {
		return _database.rawQuery("SELECT _id, beer_maker, beer_name, description FROM beers WHERE beer_name LIKE ?",
				new String[]{"%" + beerName + "%"});
	}

	public void CommissionKeg(int beerId, Location location) {
		long currentTime = System.currentTimeMillis();
		Beer b = GetBeerById(beerId);
		BeerDAO beer = new BeerDAO(b.get_beerMaker(),b.get_beerName(),b.get_beerDescription());
		KegDAO keg = new KegDAO(currentTime,location==null?0:location.getLatitude(),location==null?0:location.getLongitude(),beer);
		String cloudId = "fake";// _cloudDataAccess.CommissionKeg(keg);

		ContentValues values = new ContentValues();

		values.put("commission_time", currentTime);
		values.put("beer", beerId);
		values.put("cloud_id",cloudId);
		_database.insert("keg_events", null, values);

	}
	String GetCurrentKegCloudId() {
		//find current keg
		String returnValue = "";
		String sql = "select cloud_id from keg_events where decommission_time is NULL order by commission_time desc limit 1";
		Cursor c = _database.rawQuery(sql, null);
		if (c.moveToFirst()) // data?
		{
			returnValue = c.getString(c.getColumnIndex("cloud_id"));
		}
		c.close();
		return returnValue;
	}
	int GetCurrentBeerId() {
		//find current keg
		int returnValue = -1;
		String sql = "select beer from keg_events where decommission_time is NULL order by commission_time desc limit 1";
		Cursor c = _database.rawQuery(sql, null);
		if (c.moveToFirst()) // data?
		{
			returnValue = c.getInt(c.getColumnIndex("beer"));
		}
		c.close();
		return returnValue;
	}

	public Date GetLastPourTime() {
		long time = 0;
		String sql = "select pour_start_time from events order by pour_start_time desc limit 1";
		Cursor c = _database.rawQuery(sql, null);
		if (c.moveToFirst()) // data?
		{
			time = c.getLong(c.getColumnIndex("pour_start_time"));
		}
		c.close();
		return new Date(time);
	}

	public Beer GetBeerById(int id) {
		String sql = "select * from beers where _id = " + id;
		Cursor c = _database.rawQuery(sql, null);
		Beer b = new Beer();
		if (c.moveToFirst()) {

			b.set_beerMaker(c.getString(c.getColumnIndex("beer_maker")));
			b.set_beerName(c.getString(c.getColumnIndex("beer_name")));
			b.set_beerDescription(c.getString(c.getColumnIndex("description")));
		}
		c.close();
		return b;
	}	
	public Beer GetCurrentBeer() {
		String sql = "select * from beers where _id = " + GetCurrentBeerId();
		Cursor c = _database.rawQuery(sql, null);
		Beer b = new Beer();
		if (c.moveToFirst()) {

			b.set_beerMaker(c.getString(c.getColumnIndex("beer_maker")));
			b.set_beerName(c.getString(c.getColumnIndex("beer_name")));
			b.set_beerDescription(c.getString(c.getColumnIndex("description")));
		}
		c.close();
		return b;
	}

	public void DecommissionKeg() {
		//find current keg
		String sql = "select * from keg_events where decommission_time is NULL order by commission_time desc limit 1";
		Cursor c = _database.rawQuery(sql, null);
		if (c.moveToFirst()) // data?
		{
			int kegId = c.getInt(c.getColumnIndex("_id"));

			String strFilter = "_id=?";
			ContentValues args = new ContentValues();
			args.put("decommission_time", System.currentTimeMillis());
			_database.update("keg_events", args, strFilter, new String[]{Integer.toString(kegId)});

		}
		c.close();
	}

	public Date GetCurrentKegCommissionDate()
	{
		//find current keg
		String sql = "select commission_time from keg_events where decommission_time is NULL order by commission_time desc limit 1";
		Cursor c = null;
		
		try
		{
			c = _database.rawQuery(sql, null);
			if (c.moveToFirst()) // data?
			{
				long time = c.getLong(c.getColumnIndex("commission_time"));
				return new Date(time);
			}
		}
		catch (Exception e)
		{
			Log.e(PreferenceConstants.MY_TAG,"Error in GetCurrentKegCommissionDate");			
		}
		finally {
			if(c!=null)
				c.close();
		}
		return new Date(0);
		
		
	}
	public int GetNumberOfPoursFromCurrentKeg() {
		String sql = "select commission_time from keg_events where decommission_time is NULL order by commission_time desc limit 1";
		Cursor c = _database.rawQuery(sql, null);
		int returnValue = 0;
		long kegCommissionTime = 0;
		if (c.moveToFirst()) // data?
		{
			kegCommissionTime = c.getLong(c.getColumnIndex("commission_time"));

			sql = "select _id from events where pour_start_time > " + kegCommissionTime;
			Cursor c2 = _database.rawQuery(sql, null);
			if(c2.moveToLast())
			{
				returnValue = c2.getCount();
			}
			c2.close();
		}
		c.close();
		return returnValue;

	}
	public float GetAveragePourTimeInSeconds()
	{
		float returnValue = 0;
		String sql = "select avg(pour_length) from (select pour_end_time - pour_start_time as pour_length from events)";
		Cursor c = _database.rawQuery(sql,null);
		if(c.moveToFirst())
		{
			returnValue =  c.getFloat(0);
		}
		c.close();
		return returnValue / 1000;

	}
	public float GetTotalTapOpenTimeForCurrentKegInSeconds() {
		String sql = "select commission_time from keg_events where decommission_time is NULL order by commission_time desc limit 1";
		Cursor c = _database.rawQuery(sql, null);
		float returnValue = 0;
		long kegCommissionTime = 0;
		if (c.moveToFirst()) // data?
		{
			kegCommissionTime = c.getLong(c.getColumnIndex("commission_time"));

			sql = "select sum(pour_length) from (select pour_end_time - pour_start_time as pour_length from events where pour_start_time > "  + kegCommissionTime + ")";
			Cursor c2 = _database.rawQuery(sql, null);
			if(c2.moveToFirst())
			{
				returnValue = c2.getFloat(0);
			}
			c2.close();
		}
		c.close();
		return returnValue / 1000;

	}

	public Boolean get_initialized() {
		return _initialized;
	}

	public void set_initialized(Boolean _initialized) {
		this._initialized = _initialized;
	}
}
