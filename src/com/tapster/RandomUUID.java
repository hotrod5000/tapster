package com.tapster;

import android.util.Log;

import java.util.UUID;


/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 2/17/13
 * Time: 9:10 PM
 * Implemented this class because UUID.randomUUID() was violating strict mode
 * because apparently it goes to disk, which is a no-no on the main UI thread
 */
public class RandomUUID implements Runnable {
	UUID _id;
	public void run()
	{
		_id = UUID.randomUUID();
	}
	public  UUID GetRandomUUID()
	{
		Thread t = new Thread(this);
		t.start();
		//block until it returns

		while(t.isAlive())
		{
			Log.d(PreferenceConstants.MY_TAG,"Looping inside RandomUUID");
			try
			{
				t.join(500);
			}
			catch(InterruptedException e)
			{
				Log.e(PreferenceConstants.MY_TAG, "Shat on generating random UUID", e);
			}
		}
		return _id;
	}
}
