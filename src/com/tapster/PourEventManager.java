package com.tapster;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;


/**
 * Created by IntelliJ IDEA.
 * User: Rodney.Smith
 * Date: 1/25/12
 * Time: 8:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class PourEventManager extends BasePourEventManager implements SensorEventListener, IPourEventManager {
	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	private float azimuth;
	private float pitch;
	private float roll;
	float _closedPitch;
	float _openPitch;
	float _closedRoll;
	float _openRoll;


	boolean _tapOpen = false;
	float TapOpenTolerance = 5.0f;
	float TapClosedTolerance = 5.0f;
	int _tapOpenThresholdPercentage;

	/**
	 * Sides of the phone
	 */
	enum Side {
		TOP,
		BOTTOM,
		LEFT,
		RIGHT;
	}

	private Side _currentUpSide = null;

	public PourEventManager(SensorManager sensorManager, float closedPitch, float openPitch, float closedRoll, float openRoll) {

		mSensorManager = sensorManager;
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		_closedPitch = closedPitch;
		_openPitch = openPitch;
		_closedRoll = closedRoll;
		_openRoll = openRoll;

	}

	public void Start() {
		mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
	}

	public void Stop() {
		mSensorManager.unregisterListener(this);
	}

	public void SetTapOpenOrientation(float azimuth, float pitch, float roll) {
		Log.d(PreferenceConstants.MY_TAG, "op= " + _openPitch + " or= " + _openRoll);
		_openPitch = pitch;
		_openRoll = roll;
	}

	public void SetTapClosedOrientation(float azimuth, float pitch, float roll) {
		Log.d(PreferenceConstants.MY_TAG, "cp= " + _closedPitch + " cr= " + _closedRoll);
		_closedPitch = pitch;
		_closedRoll = roll;
	}

	public void SetTapOpenThreshold(int val) {
		_tapOpenThresholdPercentage = val;
	}

	public void onSensorChanged(SensorEvent event) {
		azimuth = event.values[0];	 // azimuth
		pitch = event.values[1];	 // pitch
		roll = event.values[2];		// roll
		if (pitch < -45 && pitch > -135) {
			// top side up
			_currentUpSide = Side.TOP;
		} else if (pitch > 45 && pitch < 135) {
			// bottom side up
			_currentUpSide = Side.BOTTOM;
		} else if (roll > 45) {
			// right side up
			_currentUpSide = Side.RIGHT;
		} else if (roll < -45) {
			// left side up
			_currentUpSide = Side.LEFT;
		}

		//if the phone is in landscape mode (_currentUpSide is Left or Right), then
		//we need to use roll as our indicator

		if (_currentUpSide == Side.LEFT || _currentUpSide == Side.RIGHT) {

			if ((Math.abs(pitch) > 90 && Math.abs(_openPitch) > 90) ||
					((Math.abs(pitch) < 90 && Math.abs(_openPitch) < 90))) {
				if (_closedRoll < _openRoll) {
					if (roll > _closedRoll + TapOpenTolerance) {
						if (!_tapOpen) {
							Log.d(PreferenceConstants.MY_TAG, "open Pitch= " + pitch + " roll= " + roll + " az= " + azimuth);
							TapOpen();
							_tapOpen = true;
						}
					} else {
						if (_tapOpen) {
							Log.d(PreferenceConstants.MY_TAG, "close Pitch= " + pitch + " roll= " + roll + " az= " + azimuth);
							TapClosed();
							_tapOpen = false;
						}
					}
				} else {
					if (roll < _closedRoll - TapOpenTolerance) {
						if (!_tapOpen) {
							Log.d(PreferenceConstants.MY_TAG, "open Pitch= " + pitch + " roll= " + roll + " az= " + azimuth);
							TapOpen();
							_tapOpen = true;
						}
					} else {
						if (_tapOpen) {
							Log.d(PreferenceConstants.MY_TAG, "close Pitch= " + pitch + " roll= " + roll + " az= " + azimuth);
							TapClosed();
							_tapOpen = false;
						}
					}
				}
			}
		} else //we are in portrait mode, phone is upright
		{
			float range = Math.abs(_closedPitch - _openPitch);
			TapOpenTolerance = range * (float) (_tapOpenThresholdPercentage * 0.01);
			if (pitch < _closedPitch - TapOpenTolerance) {
				if (!_tapOpen) {
					Log.d(PreferenceConstants.MY_TAG, "Firing TapOpen event because these conditions are met: \n" +
							"pitch=" + pitch + ", _closedPitch=" + _closedPitch + ", TapOpenTolerance=" + TapOpenTolerance + "\n" +
							"formula is pitch < _closedPitch - TapOpenTolerance");
					TapOpen();
					_tapOpen = true;
				}
			} else {
				if (_tapOpen && Math.abs(pitch - _closedPitch) < TapClosedTolerance) {
					if (_tapOpen) {
						Log.d(PreferenceConstants.MY_TAG, "Firing TapClosed event because these conditions are met: \n" +
								"pitch=" + pitch + ", _closedPitch=" + _closedPitch + ", TapClosedTolerance=" + TapClosedTolerance + "\n" +
								"formula is |pitch - _closedPitch| < TapClosedTolerance");
						TapClosed();
						_tapOpen = false;
					}
				}
			}
		}
	}

	public void onAccuracyChanged(Sensor sensor, int i) {
		//To change body of implemented methods use File | Settings | File Templates.
	}


}
