package com.tapster;

import android.hardware.SensorManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 3/9/12
 * Time: 9:58 PM
 */
abstract public class BasePourEventManager implements IPourEventManager{
	List<IPourListener> _pourListeners = new ArrayList<IPourListener>() ;
	

	public void AddListener(IPourListener listener) {
		if(!_pourListeners.contains(listener))
			_pourListeners.add(listener);
	}

	public void RemoveListener(IPourListener listener) {
		if(_pourListeners.contains(listener))
			_pourListeners.remove(listener);
	}
	public void TapOpen()
	{
		for(IPourListener l : _pourListeners){
			l.onTapOpen();
		}
	}
	public void TapClosed()
	{
		for(IPourListener l : _pourListeners){
			l.onTapClose();
		}
	}
	public void Start() {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	public void Stop() {
		//To change body of implemented methods use File | Settings | File Templates.
	}


	public void SetTapOpenOrientation(float azimuth, float pitch, float roll) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	public void SetTapClosedOrientation(float azimuth, float pitch, float roll) {
		//To change body of implemented methods use File | Settings | File Templates.
	}
}
