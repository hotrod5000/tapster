package com.tapster;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.os.Bundle;
import android.view.*;
import android.widget.*;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 2/12/12
 * Time: 8:07 PM
 */
public class ChangeKegActivity extends ListActivity {

	protected EditText searchText;
	//EventDataSQLHelper _eventsData;
	//protected SQLiteDatabase _database;
	protected Cursor cursor;
	protected ListAdapter adapter;
	protected ListView _employeeList;
	DataAccess _dataAccess;
	Location _location;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.beers);
		MainApplication app = (MainApplication) getApplication();
		_dataAccess = app.get_dataAccess();

		searchText = (EditText) findViewById (R.id.searchText);
		_employeeList = (ListView) findViewById (android.R.id.list);

		MyLocation.LocationResult locationResult = new MyLocation.LocationResult(){
			@Override
			public void gotLocation(Location location){
				//Got the location!
				_location = location;
			}
		};
		MyLocation myLocation = new MyLocation();
		myLocation.getLocation(this, locationResult);



	}
	public void onListItemClick(ListView parent, View view, int position, long id) {
		Cursor cursor = (Cursor) adapter.getItem(position);
		final int beerId = cursor.getInt(cursor.getColumnIndex("_id"));
		String beerName =   cursor.getString(cursor.getColumnIndex("beer_name"));
		final AlertDialog.Builder b = new AlertDialog.Builder(this);
		b.setIcon(android.R.drawable.ic_dialog_alert);
		b.setTitle("Change keg");
		b.setMessage("Commission a keg of " + beerName + "?");

		b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				_dataAccess.CommissionKeg(beerId,_location);
                finish();
			}
		});
		b.setNegativeButton(android.R.string.no, null);
		b.show();
	}

	public void search(View view) {
		// || is the concatenation operation in SQLite
		cursor = _dataAccess.GetBeersWithNameLike(searchText.getText().toString());
		adapter = new SimpleCursorAdapter(
				this,
				R.layout.beer_list_item,
				cursor,
				new String[] {"beer_maker", "beer_name", "description"},
				new int[] {R.id.beerMaker, R.id.beerName, R.id.beerDescription});
		_employeeList.setAdapter(adapter);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.add_beer, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {

			case R.id.add_beer:
				AddBeer();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void AddBeer() {
		LayoutInflater factory = LayoutInflater.from(this);
		final View textEntryView = factory.inflate(R.layout.add_beer,  (ViewGroup) findViewById(R.id.dialog_layout_root));

		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Add a beer");
		alert.setMessage("Enter the details about the beer.");

// Set an EditText view to get user input
		//final EditText input = new EditText(this);

		alert.setView(textEntryView);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				EditText maker = (EditText) textEntryView.findViewById(R.id.beerMaker);
				String beerMaker = maker.getText().toString();

				EditText name = (EditText) textEntryView.findViewById(R.id.beerName);
				String beerName = name.getText().toString();

				EditText desc = (EditText) textEntryView.findViewById(R.id.beerDescription);
				String beerDesc = desc.getText().toString();

				AddBeerToDatabase(beerMaker,beerName,beerDesc);

			}
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.
			}
		});

		alert.show();
	}
	private void AddBeerToDatabase(String beerMaker, String beerName, String description){
		_dataAccess.AddBeer(beerMaker, beerName, description);
	}


}