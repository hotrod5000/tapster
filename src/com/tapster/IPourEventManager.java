package com.tapster;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 2/2/12
 * Time: 8:36 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IPourEventManager {
    void Start();
    void Stop();
	void AddListener(IPourListener listener);
	void RemoveListener(IPourListener listener);
    void SetTapOpenOrientation(float azimuth, float pitch, float roll);
    void SetTapClosedOrientation(float azimuth, float pitch, float roll);

    void SetTapOpenThreshold(int val);
}
