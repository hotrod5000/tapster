package com.tapster;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;
import com.tapster.modules.DefaultModule;
import dagger.ObjectGraph;

import java.io.*;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 2/11/12
 * Time: 3:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class MainApplication extends Application {

	private ObjectGraph objectGraph;
    private IPourEventManager _pourEventManager;
	DataAccess _dataAccess;

	public ObjectGraph getObjectGraph() {
		return this.objectGraph;
	}

    @Override
    public void onCreate() {
        super.onCreate();
        copyAssets();
		this.objectGraph = ObjectGraph.create(new DefaultModule());
		// Activate StrictMode
//		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//				.detectAll().penaltyLog().penaltyDeath().build());
//		StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll()
//				.penaltyLog().penaltyDeath().build());

        if(isEmulator())
        {
            Log.d(PreferenceConstants.MY_TAG,"Running in emulator");
            _pourEventManager = new NullPourEventManager();
        }
        else
        {
            SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(this);

            float openPitch = p.getFloat(PreferenceConstants.OpenPitch, -90);
            float closedPitch = p.getFloat(PreferenceConstants.ClosedPitch, -100);
			float openRoll = p.getFloat(PreferenceConstants.OpenRoll, -90);
			float closedRoll = p.getFloat(PreferenceConstants.ClosedRoll, -100);
            int threshold = p.getInt(PreferenceConstants.TapOpenThreshold, 20);
            _pourEventManager = new PourEventManager((SensorManager)getSystemService(Context.SENSOR_SERVICE),
                    closedPitch,
                    openPitch,
					closedRoll,
					openRoll
					);
            _pourEventManager.SetTapOpenThreshold(threshold);
			if(p.contains(PreferenceConstants.OpenPitch) && p.contains(PreferenceConstants.ClosedPitch))
			{
				_pourEventManager.Start();
			}

        }
        _dataAccess = new DataAccess(this);
    }

    private void copyAssets() {
        String soundsFolder = "audio_clips";
        AssetManager assetManager = getAssets();
        String[] files = null;
        try {
            files = assetManager.list(soundsFolder);
        } catch (IOException e) {
            Log.e(PreferenceConstants.MY_TAG, "Failed to get asset file list.", e);
        }
        for(String filename : files) {
            InputStream in = null;
            OutputStream out = null;
            try {
                String n = getExternalFilesDir(null).getPath();
                Log.d(PreferenceConstants.MY_TAG,"getExternalFilesDir = " + n);
                in = assetManager.open(soundsFolder + "/" + filename);
                new File(getExternalFilesDir(null).getPath() +"/" + soundsFolder).mkdirs();
                File outFile = new File(getExternalFilesDir(null).getPath() +"/" + soundsFolder, filename);
                if(outFile.exists()){
                    Log.d(PreferenceConstants.MY_TAG, filename + " file already exists at " + outFile.getAbsolutePath());
                }
                else
                {
                    Log.d(PreferenceConstants.MY_TAG,"copying " + filename + " file to " + outFile.getAbsolutePath());
                    out = new FileOutputStream(outFile);
                    copyFile(in, out);
                    out.flush();
                    out.close();
                }
                in.close();
            } catch(Exception e) {
                Log.e(PreferenceConstants.MY_TAG, "Failed to copy asset file: " + filename, e);
            }
        }
    }
    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }
    public DataAccess get_dataAccess(){
		return this._dataAccess;
	}
    public IPourEventManager get_pourEventManager() {
        return this._pourEventManager;
    }
    public boolean isEmulator() {
        return Build.MANUFACTURER.equals("unknown");
    }
	@Override
	public void onTerminate(){
		_pourEventManager.Stop();
		_dataAccess.Close();

	}
}

