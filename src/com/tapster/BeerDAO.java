package com.tapster;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 10/28/12
 * Time: 7:56 AM
 */
public class BeerDAO {
	public String BeerMaker = "";
	public String BeerName = "";
	public String BeerDescription = "";

	public BeerDAO(String beerMaker,String beerName,String beerDescription){
		BeerMaker = beerMaker;
		BeerName = beerName;
		BeerDescription = beerDescription;
	}
}
