package com.tapster.modules;

import com.tapster.Dashboard;
import com.tapster.DataAccess;
import com.tapster.utils.ILog;
import com.tapster.utils.MyLog;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 3/1/13
 * Time: 10:46 PM
 */
@Module(entryPoints = Dashboard.class)
public class DefaultModule {
	@Provides
	ILog provideLog() {
		return new MyLog();
	}

}
