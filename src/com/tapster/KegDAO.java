package com.tapster;



/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 10/28/12
 * Time: 6:53 AM
 */
public class KegDAO {
	public long BeginTime, EndTime;
	public double Latitude, Longitude;
	public BeerDAO Beer;
	public PourDAO[] Pours = new PourDAO[0];
	public KegDAO(long beginTime, double latitude, double longitude, BeerDAO beer){
		BeginTime = beginTime;
		Latitude = latitude;
		Longitude = longitude;
		Beer = beer;

	}

}
