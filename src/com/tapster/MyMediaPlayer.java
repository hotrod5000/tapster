package com.tapster;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 3/19/12
 * Time: 9:41 PM
 */
public class MyMediaPlayer implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener{
	//set up MediaPlayer
	MediaPlayer mp = new MediaPlayer();
	
	File _mediaDirectory;
    ArrayList<File> _files = new ArrayList<File>();
	int _counter = 0;
	Context _context;
	public MyMediaPlayer(Context c,String mediaDirectory)
	{
		_context = c;
		mp.setOnCompletionListener(this);
		_mediaDirectory = new File( mediaDirectory);
        if(!_mediaDirectory.exists())
        {
            Log.i(PreferenceConstants.MY_TAG, "Media directory '" + mediaDirectory +"' does not exist" );
            return;
        }
		File[] fs = _mediaDirectory.listFiles();

		for(File f : fs )
		{
			if(f.isFile())
                _files.add(f);
		}

	}
	private File getNextFileFromMediaDirectory()
	{
		if(_counter > _files.size() - 1)
			_counter = 0;
        return  _files.get(_counter++);
	}
	public void Play(){
        if(_files.size() == 0) return;
		String mediaFile = getNextFileFromMediaDirectory().getPath();
		try {
			if(mp.isPlaying())
			{
				mp.stop();
				mp.reset();
			}
			mp.setDataSource(mediaFile);
		} catch (IllegalArgumentException e) {
			Toast.makeText(_context,"Error playing media file " + mediaFile,Toast.LENGTH_LONG ).show();
			Log.e(PreferenceConstants.MY_TAG, "Error playing media file " + mediaFile, e);
		} catch (IllegalStateException e) {
			Toast.makeText(_context,"Error playing media file " + mediaFile,Toast.LENGTH_LONG ).show();
			Log.e(PreferenceConstants.MY_TAG, "Error playing media file " + mediaFile, e);
		} catch (IOException e) {
			Toast.makeText(_context,"Error  playing media file " + mediaFile,Toast.LENGTH_LONG ).show();
			Log.e(PreferenceConstants.MY_TAG, "Error playing media file " + mediaFile, e);
		}
		try {
			mp.prepare();
		} catch (IllegalStateException e) {
			Toast.makeText(_context,"Error preparing media file " + mediaFile,Toast.LENGTH_LONG ).show();
			Log.e(PreferenceConstants.MY_TAG, "Error preparing media file " + mediaFile, e);
		} catch (IOException e) {
			Toast.makeText(_context,"Error preparing media file " + mediaFile,Toast.LENGTH_LONG ).show();
			Log.e(PreferenceConstants.MY_TAG, "Error preparing media file " + mediaFile, e);
		}
		mp.start();
		
	}

	public void Stop()
	{
		mp.stop();
		mp.reset();
	}

	public void onCompletion(MediaPlayer mediaPlayer) {
		mp.reset();
	}


	public void onPrepared(MediaPlayer mediaPlayer) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
		Log.e(PreferenceConstants.MY_TAG, "Crap");
		return false;  //To change body of implemented methods use File | Settings | File Templates.
	}
}
