package com.tapster;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 2/12/12
 * Time: 5:18 PM
 * To change this template use File | Settings | File Templates.
 */

//snagged from here http://marakana.com/forums/android/examples/55.html


public class PourLogActivity extends Activity {
	DataAccess _dataAccess;
	final int PICK_CONTACT = 66;
	Cursor cursor;
	String _pourLog = "";
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

		setContentView(R.layout.pourlog);
		MainApplication app = (MainApplication) getApplication();
		_dataAccess = app.get_dataAccess();
		cursor = _dataAccess.GetAllPourEvents();

		startManagingCursor(cursor); //this is for if you turn this into a list activity and bind i think
		showEvents();


    }
	public void onStop()
	{
		stopManagingCursor(cursor);
		cursor.close();
		super.onStop();
	}
	@Override
	public void onActivityResult(int reqCode, int resultCode, Intent data) {
		super.onActivityResult(reqCode, resultCode, data);
		String phone = "", email = "";
		switch (reqCode) {
			case (PICK_CONTACT) :
				if (resultCode == Activity.RESULT_OK) {
					Uri contactData = data.getData();
					Cursor c =  managedQuery(contactData, null, null, null, null);
					if (c.moveToFirst()) {
						String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
						String id = c.getString(
								c.getColumnIndex(ContactsContract.Contacts._ID));
						if (Integer.parseInt(c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
								Cursor pCur = managedQuery(
										ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
										null,
										ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
										new String[]{id}, null);
								while (pCur.moveToNext()) {
									     phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA))  ;
								}
								pCur.close();
						}
						Cursor emailCur = managedQuery(
								ContactsContract.CommonDataKinds.Email.CONTENT_URI,
								null,
								ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
								new String[]{id}, null);
						while (emailCur.moveToNext()) {
							// This would allow you get several email addresses
							// if the email addresses were stored in an array
							email = emailCur.getString(
									emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
							String emailType = emailCur.getString(
									emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
						}
						emailCur.close();
					}
					c.close();
				}
				break;
		}
		if(email != "")
		{
			try {
				GmailSender sender = new GmailSender("marsallus.wallace@gmail.com", "Pertrac1");
				sender.sendMail("Tapster pour log",_pourLog,"marsallus.wallace@gmail.com", email);
			} catch (Exception e) {
				Log.e(PreferenceConstants.MY_TAG, e.getMessage(), e);
			}
		}
	}
	public void SendPourReport(View v)
	{
		Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
		startActivityForResult(intent, PICK_CONTACT);
	}

	private void showEvents() {
		StringBuilder ret = new StringBuilder("Saved Events:\n\n");
		while (cursor.moveToNext()) {
			long id = cursor.getLong(0);
			long startTime = cursor.getLong(1);
			SimpleDateFormat timingFormat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
			String startTimestamp = timingFormat.format(new Date(startTime));

            long stopTime = cursor.getLong(2);
            int millis = (int)(stopTime - startTime);
            float seconds = millis * 0.001f;
			ret.append(id + ": " + startTimestamp + ": " + seconds + "\n");
		}
		_pourLog = ret.toString();
		TextView v = (TextView) this.findViewById(R.id.PourLogTextView);
		v.setText(ret);
	}
	
}