package com.tapster;


import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import com.cedarsoftware.util.io.JsonReader;
import com.cedarsoftware.util.io.JsonWriter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 1/12/13
 * Time: 8:14 AM
 */
public class CodeSmithDataAccess {
	final DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	Context _context;
	String _apiKey;
	Location _location;
	DataAccess _da;
	public CodeSmithDataAccess(Context c, DataAccess da)
	{

		_context = c;
		_da = da;
		CaptureLocation();

	}
	void CaptureLocation()
	{
		MyLocation.LocationResult locationResult = new MyLocation.LocationResult(){
			@Override
			public void gotLocation(Location location){
				//Got the location!
				_location = location;
				Log.d(PreferenceConstants.MY_TAG, "Location captured. lat=" + _location.getLatitude() + "long="+_location.getLongitude());
			}
		};
		MyLocation myLocation = new MyLocation();
		myLocation.getLocation(_context, locationResult);
	}
	//returns id of newly written pour event, or -1 otherwise
	public void WritePourEvent(UUID pourIdFromSource, Date start, Date end) {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(_context);
		String pourUrl = settings.getString(PreferenceConstants.PourUrl, "");
		String body;
		int pourId = - 1;
		Map<String, String> pourMap = new HashMap<String,String>();
		try{
			pourMap.put("\"Start\"", fmt.format(start) );
			pourMap.put("\"End\"", fmt.format(end) );
			pourMap.put("\"ApiKey\"", _apiKey );
			pourMap.put("\"Beverage\"", "wtf" );

			String jsonString = "\"Start\":\"" +
					fmt.format(start) +
					"\",\"End\":\"" +
					fmt.format(end) +
					"\",\"ApiKey\":\"" +
					_apiKey +

					"\",\"Beverage\":\"" +
					_da.GetCurrentBeer().get_beerName() +
					"\",\"PourIdFromSource\":\"" +
					pourIdFromSource.toString() +
					"\"";

			if(_location != null)
			{
				pourMap.put("\"Latitude\"", _location.getLatitude() + "" );
				pourMap.put("\"Longitude\"", _location.getLongitude() + ""  );
				String locationProps = "\",\"Latitude\":\"" +
						_location.getLatitude() +
						"\",\"Longitude\":\"" ;
				jsonString = jsonString + " " + locationProps;

			}
			jsonString = "{" + jsonString + "}";
			Log.i(PreferenceConstants.MY_TAG, "POST to url " + pourUrl);
			Log.i(PreferenceConstants.MY_TAG, "theJSON is " + jsonString);
			HttpResponse resp = makePostRequest(pourUrl, jsonString);
			HttpEntity responseEntity = resp.getEntity();
			if(responseEntity!=null) {
				body = EntityUtils.toString(responseEntity);
				Log.i(PreferenceConstants.MY_TAG,"Response: " + body);
				Map r = JsonReader.jsonToMaps(body);
				if(r.containsKey("Id"))
				{
					pourId = Integer.parseInt( r.get("Id").toString() );
				}
			}
			else
			{

				Log.i(PreferenceConstants.MY_TAG, "HttpResponse entity is null");
			}
		}
		catch(IOException ioe)
		{
			Log.e(PreferenceConstants.MY_TAG, "error writing pour event to codesmith", ioe);
		}
		catch (Exception e){
			Log.e(PreferenceConstants.MY_TAG, "error writing pour event to codesmith", e);
		}

	}
	public static HttpResponse makePostRequest(String path, String json) throws Exception
	{
		//instantiates httpclient to make request
		DefaultHttpClient httpclient = new DefaultHttpClient();

		//url with the post data
		HttpPost httpost = new HttpPost(path);



		//passes the results to a string builder/entity
		StringEntity se = new StringEntity(json);

		//sets the post request as the resulting string
		httpost.setEntity(se);
		//sets a request header so the page receiving the request
		//will know what to do with it
		httpost.setHeader("Accept", "application/json");
		httpost.setHeader("Content-type", "application/json");

		return httpclient.execute(httpost);
	}

	public void DiscoverUrls()
	{
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(_context);
		_apiKey = settings.getString("pref_api_key", "");
		String discoveryUrl = settings.getString("pref_discovery_url","");
		if(_apiKey == "")
		{
			Log.i(PreferenceConstants.MY_TAG, "Unable to retrieve web api URL's because no Api Key has been entered");
			return;
		}
		DefaultHttpClient httpclient = new DefaultHttpClient();
		String url = discoveryUrl + "?key=" + _apiKey;
		HttpGet httpGet = new HttpGet(url);
		try
		{
			Log.d(PreferenceConstants.MY_TAG, "DiscoverUrls executing http GET against " + url);
			HttpResponse resp = httpclient.execute(httpGet);

			HttpEntity responseEntity = resp.getEntity();
			if(responseEntity!=null) {
				String body = EntityUtils.toString(responseEntity);
				Map m = JsonReader.jsonToMaps(body);
				if(m.containsKey("Pour"))
				{

					SharedPreferences.Editor editor = settings.edit();
					String pourUrl = m.get("Pour").toString();
					editor.putString(PreferenceConstants.PourUrl, pourUrl);

					// Commit the edits!
					editor.commit();

					Log.i(PreferenceConstants.MY_TAG,"PourUrl= " + pourUrl);
				}
				Log.i(PreferenceConstants.MY_TAG,"Response: " + body);
			}


		}
		catch (IOException ioe)
		{
			Log.d(PreferenceConstants.MY_TAG, "failed to discover api urls");

		}
	}
	public void UploadFile(UUID pourId, String fileName, MediaType mediaType)
	{
		try

		{
			String url = "http://codesmith.azurewebsites.net/api/MediaFileApi";
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);

			FileBody bin = new FileBody(new File(fileName));

			MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

			reqEntity.addPart("bin", bin);
			reqEntity.addPart("pourId", new StringBody(pourId.toString()));
			reqEntity.addPart("mediaType", new StringBody(mediaType.toString()));
			httppost.setEntity(reqEntity);

			HttpResponse response = httpclient.execute(httppost);
			HttpEntity resEntity = response.getEntity();
			Log.d(PreferenceConstants.MY_TAG, "response from uploading file: " + EntityUtils.toString(resEntity));
		}
		catch (Exception e)
		{
			Log.e(PreferenceConstants.MY_TAG, "Error uploading file " + fileName, e);
		}
	}

}
