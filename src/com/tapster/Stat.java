package com.tapster;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 3/2/12
 * Time: 10:09 PM
 */
public class Stat {
	private String statName;
	private String statValue;

	public Stat(){
		
	}
	public Stat(String name, String value){
		statName = name;
		statValue = value;
	}
	public String getStatName() {
		return statName;
	}
	public void setStatName(String statName) {
		this.statName = statName;
	}
	public String getStatValue() {
		return statValue;
	}
	public void setStatValue(String statValue) {
		this.statValue = statValue;
	}
}
