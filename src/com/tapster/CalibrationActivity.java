package com.tapster;

import android.app.Activity;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 1/23/12
 * Time: 9:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class CalibrationActivity extends Activity implements SensorEventListener,SeekBar.OnSeekBarChangeListener
{
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private float azimuth;
    private float pitch;
    private float roll;
    IPourEventManager _pourEventManager;
    TextView mProgressText;
    SeekBar mSeekBar;
    public CalibrationActivity() {

    }

    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calibration);
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        MainApplication app = (MainApplication) getApplication();
        _pourEventManager = app.get_pourEventManager();
        mProgressText = (TextView)findViewById(R.id.ThresholdValue);
        mSeekBar = (SeekBar)findViewById(R.id.Threshold);
        mSeekBar.setOnSeekBarChangeListener(this);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int openThreshold = prefs.getInt(PreferenceConstants.TapOpenThreshold,20);
        mSeekBar.setProgress(openThreshold);




    }

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		StoreSliderValue();
	}
	int StoreSliderValue()
	{
		int val = mSeekBar.getProgress();
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(PreferenceConstants.TapOpenThreshold, val);
		// Commit the edits!
		editor.commit();
		return val;
	}
	@Override
    public void onStop()
    {
		int sliderValue = StoreSliderValue();
        _pourEventManager.SetTapOpenThreshold(sliderValue);
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		//if they have calibrated, then start the pour event manager
		if(settings.contains(PreferenceConstants.OpenPitch) && settings.contains(PreferenceConstants.ClosedPitch))
		{
			_pourEventManager.Start();
		}

        super.onStop();

    }
    public void StoreOpenPref(View view) {

        TextView v = (TextView) this.findViewById(R.id.CalibrationTextViewOpen);
        StringBuilder sb = new StringBuilder();
        sb.append("----------------\n");
        sb.append("Open settings:\n");
        sb.append("azimuth= ").append(azimuth).append("\n");
        sb.append("pitch= ").append(pitch).append("\n");
        sb.append("roll= ").append(roll).append("\n");
        v.setText(sb.toString());


        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat(PreferenceConstants.OpenPitch, pitch);
        editor.putFloat(PreferenceConstants.OpenRoll, roll);
        editor.putFloat(PreferenceConstants.OpenAzimuth, azimuth);
        _pourEventManager.SetTapOpenOrientation(azimuth,pitch,roll);
        // Commit the edits!
        editor.commit();
        sb.append("Stored!");
        v.setText(sb.toString());

    }

    public void StoreClosedPref(View view) {

        TextView v = (TextView) this.findViewById(R.id.CalibrationTextViewClosed);
        StringBuilder sb = new StringBuilder();
        sb.append("----------------\n");
        sb.append("Closed settings:\n");
        sb.append("azimuth= ").append(azimuth).append("\n");
        sb.append("pitch= ").append(pitch).append("\n");
        sb.append("roll= ").append(roll).append("\n");
        v.setText(sb.toString());


        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat(PreferenceConstants.ClosedPitch, pitch);
        editor.putFloat(PreferenceConstants.ClosedRoll, roll);
        editor.putFloat(PreferenceConstants.ClosedAzimuth, azimuth);
        _pourEventManager.SetTapClosedOrientation(azimuth,pitch,roll);
        // Commit the edits!
        editor.commit();
        sb.append("Stored!");
        v.setText(sb.toString());

    }

    public void onSensorChanged(SensorEvent event) {
        azimuth = event.values[0];     // azimuth
        pitch = event.values[1];     // pitch
        roll = event.values[2];        // roll
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        mProgressText.setText("Tap is considered open when handle is " + i + "% open.");
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
