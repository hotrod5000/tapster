package com.tapster.utils;

import android.util.Log;
import com.tapster.PreferenceConstants;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 3/1/13
 * Time: 10:39 PM
 */
public class MyLog implements ILog {

	public void d(String message){
		Log.d(PreferenceConstants.MY_TAG, message);
	}
	public void e(String message, Throwable error){
		Log.e(PreferenceConstants.MY_TAG, message, error);
	}
}
