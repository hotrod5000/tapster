package com.tapster.utils;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 3/1/13
 * Time: 10:43 PM
 */
public class NullLog implements ILog {
	public void d(String message) {
	}

	public void e(String message, Throwable error) {
	}
}
