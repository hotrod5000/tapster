package com.tapster.utils;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 3/1/13
 * Time: 10:43 PM
 */
public interface ILog {
	void d(String message);

	void e(String message, Throwable error);
}
