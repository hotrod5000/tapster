package com.tapster;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney.Smith
 * Date: 2/14/12
 * Time: 8:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class Beer {
    private String _beerMaker = "";
    private String _beerName = "";
    private String _beerDescription = "";


    public String get_beerDescription() {
        return _beerDescription;
    }

    public void set_beerDescription(String _beerDescription) {
        this._beerDescription = _beerDescription;
    }

    public String get_beerMaker() {
        return _beerMaker;
    }

    public void set_beerMaker(String _beerMaker) {
        this._beerMaker = _beerMaker;
    }

    public String get_beerName() {
        return _beerName;
    }

    public void set_beerName(String _beerName) {
        this._beerName = _beerName;
    }
}
