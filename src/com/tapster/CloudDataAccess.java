package com.tapster;

import android.util.Log;
import com.cedarsoftware.util.io.JsonReader;
import com.cedarsoftware.util.io.JsonWriter;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 10/28/12
 * Time: 7:51 AM
 */
public class CloudDataAccess {
	final String _token = "arxtf9bds4khb4ejtvee";

	public String CommissionKeg(KegDAO keg){
		String body;
		String cloudId="";
		String url = "https://api.mongohq.com/databases/Tapster/collections/kegs/documents?_apikey=" + _token;
		try{
			MongoDoc md = new MongoDoc(keg);
			String jsonString = JsonWriter.objectToJson(md);
			Log.i(PreferenceConstants.MY_TAG, "Commissioning keg: " + jsonString);
			HttpResponse resp = makePostRequest(url, jsonString);
			HttpEntity responseEntity = resp.getEntity();
			if(responseEntity!=null) {
				body = EntityUtils.toString(responseEntity);
				Map m = JsonReader.jsonToMaps(body);
				if(m.containsKey("_id"))
				{
					cloudId = m.get("_id").toString();
				}
				Log.i(PreferenceConstants.MY_TAG,"Response: " + body);
			}
		}
		catch(Exception e){
			Log.e(PreferenceConstants.MY_TAG, "Error serializing KegDAO to json",e);
		}
		
		return cloudId;
	}
	public HttpResponse makePostRequest(String path, String json) throws Exception
	{
		//instantiates httpclient to make request
		DefaultHttpClient httpclient = new DefaultHttpClient();

		//url with the post data
		HttpPost httpost = new HttpPost(path);



		//passes the results to a string builder/entity
		StringEntity se = new StringEntity(json);

		//sets the post request as the resulting string
		httpost.setEntity(se);
		//sets a request header so the page receiving the request
		//will know what to do with it
		httpost.setHeader("Accept", "application/json");
		httpost.setHeader("Content-type", "application/json");

		return httpclient.execute(httpost);
	}
	public HttpResponse makePutRequest(String path, String json) throws Exception
	{
		//instantiates httpclient to make request
		DefaultHttpClient httpclient = new DefaultHttpClient();

		//url with the post data
		HttpPut httput = new HttpPut(path);
		//passes the results to a string builder/entity
		StringEntity se = new StringEntity(json);

		//sets the post request as the resulting string
		httput.setEntity(se);
		//sets a request header so the page receiving the request
		//will know what to do with it
		httput.setHeader("Accept", "application/json");
		httput.setHeader("Content-type", "application/json");

		return httpclient.execute(httput);
	}
	public void WritePourEvent(long start, long end, String kegDocId) {

		String pourEvent = "{\"document\" : { \"$push\" : {\"Pours\":{\"StartTime\":[StartTime],\"EndTime\":[EndTime]}} } }";
		String url = "https://api.mongohq.com/databases/Tapster/collections/kegs/documents/" + kegDocId + "?_apikey=" + _token;
		String json = pourEvent.replace("[StartTime]",start + "");
		json = json.replace("[EndTime]",end + "");
		String body;

		Log.i(PreferenceConstants.MY_TAG,"Pour event:" + json);
		try{
			HttpResponse resp = makePutRequest(url, json);
			HttpEntity responseEntity = resp.getEntity();
			if(responseEntity!=null) {
				body = EntityUtils.toString(responseEntity);
				Log.i(PreferenceConstants.MY_TAG,"Response: " + body);
			}
		}
		catch(Exception e){
			Log.e(PreferenceConstants.MY_TAG, "Error serializing pourDAO to json",e);
		}


	}
}
