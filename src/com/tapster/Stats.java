package com.tapster;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 3/2/12
 * Time: 10:58 PM
 */
public class Stats {

	DataAccess _da;

	public Stats(DataAccess da) {
		_da = da;
	}

	public Stat GetLastPourTime() {
		Date d = _da.GetLastPourTime();
		Stat returnValue = new Stat();
		returnValue.setStatName("Last pour time");
		SimpleDateFormat timingFormat = new SimpleDateFormat("E MMM dd HH:mm yyyy");
		String startTimestamp = timingFormat.format(d);
		returnValue.setStatValue(startTimestamp);
		return returnValue;

	}

	public Stat GetTimeSinceLastPour() {
		Stat returnValue = new Stat();
		returnValue.setStatName("Time since last pour");
		String val = Timespan(_da.GetLastPourTime(), new Date());
		returnValue.setStatValue(val);
		return returnValue;
	}

	String Timespan(Date start, Date end) {

		long diffInSeconds = (end.getTime() - start.getTime()) / 1000;

		long diff[] = new long[]{0, 0, 0, 0};
		/* sec */
		diff[3] = (diffInSeconds >= 60 ? diffInSeconds % 60 : diffInSeconds);
		/* min */
		diff[2] = (diffInSeconds = (diffInSeconds / 60)) >= 60 ? diffInSeconds % 60 : diffInSeconds;
		/* hours */
		diff[1] = (diffInSeconds = (diffInSeconds / 60)) >= 24 ? diffInSeconds % 24 : diffInSeconds;
		/* days */
		diff[0] = (diffInSeconds = (diffInSeconds / 24));

		String ret = String.format(
				"%d day%s, %d hour%s, %d minute%s, %d second%s ago",
				diff[0],
				diff[0] > 1 ? "s" : "",
				diff[1],
				diff[1] > 1 ? "s" : "",
				diff[2],
				diff[2] > 1 ? "s" : "",
				diff[3],
				diff[3] > 1 ? "s" : "");
		return ret;
	}

	public Stat GetNumberOfPoursFromCurrentKeg() {
		return new Stat("Number of pours from this keg", _da.GetNumberOfPoursFromCurrentKeg() + "");
	}

	public Stat GetCurrentKegCommissionDate() {
		Date d = _da.GetCurrentKegCommissionDate();
		SimpleDateFormat timingFormat = new SimpleDateFormat("MMM dd, yyyy");
		String startTimestamp = timingFormat.format(d);
		return new Stat("Current keg commissioned on", startTimestamp);
	}
	public Stat GetAveragePourTimeInSeconds()
	{
		float time = _da.GetAveragePourTimeInSeconds();
		return new Stat("Average pour time",String.format("%.1f",time) + " seconds");
	}
	public Stat GetTotalTapOpenTimeForCurrentKegInSeconds()
	{
		float time = _da.GetTotalTapOpenTimeForCurrentKegInSeconds();
		float timeInMinutes = time / 60;

		return new Stat("This keg has been poured for", String.format("%.1f",timeInMinutes) + " minutes");
	}
}
