package com.tapster;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by IntelliJ IDEA.
 * User: Rodney
 * Date: 2/13/12
 * Time: 9:03 PM
 */
public class EmptyKegActivity extends Activity {

	DataAccess _dataAccess;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.empty_keg);
		MainApplication app = (MainApplication) getApplication();
		_dataAccess = app.get_dataAccess();

		_dataAccess.DecommissionKeg();

		sendEmail();
		finish();

	}
	private void sendEmail() {
		try {
			GmailSender sender = new GmailSender("marsallus.wallace@gmail.com", "Pertrac1");
			sender.sendMail("KEG IS EMPTY!",
					"Dude, we need more beer",
					"marsallus.wallace@gmail.com",
					"smithrod5000@gmail.com");
		} catch (Exception e) {
			Log.e(PreferenceConstants.MY_TAG, e.getMessage(), e);
		}
	}

}