package com.tapster;


import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;
import android.widget.Toast;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;

/** Helper to the database, manages versions and creation */
public class EventDataSQLHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "events.db";
    private static final int DATABASE_VERSION = 1;

    // Table name
    public static final String TABLE = "events";
	public static final String BEERS_TABLE = "beers";

    // Columns
    public static final String TIME = "time";
    public static final String TITLE = "title";
	Context _context;
    public EventDataSQLHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
		_context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
		CreateBeersTable(db);
		CreatePourTable(db);
		CreateKegTable(db);
    }

	private void CreateKegTable(SQLiteDatabase db) {
		String sql;
		sql = "create table if not exists keg_events " + "( " + BaseColumns._ID
				+ " integer primary key autoincrement, "
				+ "commission_time INTEGER not null,"
				+ "decommission_time INTEGER,"
				+ "beer INTEGER,"
				+ "cloud_id text,"
				+ "FOREIGN KEY(beer) REFERENCES beers(_id)"
				+ ");";
		Log.d("EventsData", "onCreate: " + sql);
		db.execSQL(sql);
	}

	private void CreatePourTable(SQLiteDatabase db) {
		String sql;
		sql = "create table if not exists " + TABLE + "( " + BaseColumns._ID
				+ " integer primary key autoincrement, "
                + "pour_start_time" + " integer not null, "
				+ "pour_end_time" + " integer,"
				+ "beer INTEGER,"
				+ "FOREIGN KEY(beer) REFERENCES beers(_id)"
				+ ");";
		Log.d("EventsData", "onCreate: " + sql);
		db.execSQL(sql);
	}

	private void CreateBeersTable(SQLiteDatabase db) {

		String s;
		try {
			InputStream in = _context.getResources().openRawResource(R.raw.sql);
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = builder.parse(in, null);
			NodeList statements = doc.getElementsByTagName("statement");
			for (int i=0; i<statements.getLength(); i++) {
				s = statements.item(i).getChildNodes().item(0).getNodeValue();
				db.execSQL(s);
			}
		} catch (Throwable t) {
			Log.e(PreferenceConstants.MY_TAG, "error creating beers table", t);
			Toast.makeText(_context, t.toString(), 50000).show();
		}

	}


	@Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion >= newVersion)
            return;

        String sql = null;
        if (oldVersion == 1)
            sql = "alter table " + TABLE + " add note text;";
        if (oldVersion == 2)
            sql = "";

        Log.d("EventsData", "onUpgrade	: " + sql);
        if (sql != null)
            db.execSQL(sql);
    }

}