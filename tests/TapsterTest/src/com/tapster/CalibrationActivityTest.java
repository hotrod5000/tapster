package com.tapster;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.SeekBar;

/**
 * This is a simple framework for a test of an Application.  See
 * {@link android.test.ApplicationTestCase ApplicationTestCase} for more information on
 * how to write and extend Application tests.
 * <p/>
 * To run this test, you can type:
 * adb shell am instrument -w \
 * -e class com.tapster.CalibrationActivityTest \
 * com.tapster.tests/android.test.InstrumentationTestRunner
 */
public class CalibrationActivityTest extends ActivityInstrumentationTestCase2<CalibrationActivity> {

	CalibrationActivity activity;
	public CalibrationActivityTest() {
		super("com.tapster", CalibrationActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		activity = getActivity();



	}

	public void testSeekBarMaintainsState(){
		SeekBar bar = (SeekBar)activity.findViewById(R.id.Threshold);
		// Set the bar to a known position
		bar.setProgress(20);

		// Stop the activity - The onDestroy() method should save the state of the SeekBar
		activity.finish();

		// Re-start the Activity - the onResume() method should restore the state of the Spinner
		activity = getActivity();
		bar = (SeekBar)activity.findViewById(R.id.Threshold);
		// Get the Spinner's current position
		int currentPosition = bar.getProgress();

		// Assert that the current position is the same as the starting position
		assertEquals(20, currentPosition);


	}
//	public void test2(){
//		assertTrue(false);
//	}
//	public void test3(){
//		assertTrue(true);
//	}


}
